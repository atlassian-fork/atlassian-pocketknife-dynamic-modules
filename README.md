
    "We will decide what modules come to this plugin and the circumstances in which they come!"

         -  John Howard - Dog Whistling 2002


Atlassian Pocketknife Dynamic Modules allows you as a plugin author to be in control of what functionality 
you expose the world and when.

It also has support for finding optional services in an dynamic OSGi world.

# How to use it #

Say for example say you have v5 and v6 support of a product but only some modules
are available in v6.  

With this code YOU get to decide what modules are published.  This allows you to bridge the gap between major versions
say with dynamic detection at runtime.
  
You call by creating a `LoaderConfiguration`, which indicates where your xml resources are and dynamically what
files you want included as dynamic modules.
  
    private PluginAccessor pluginAccessor; // Spring injected
    private DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory; // Spring injected

    private ModuleRegistrationHandle registrationHandle;

    void onStart() {
        Plugin plugin = pluginAccessor.getPlugin("your.plugin.key");
        final LoaderConfiguration config = new LoaderConfiguration(plugin);
        config.addPathsToAuxAtlassianPluginXMLs(
                "/dynamic/define-servlets.xml",
                "/dynamic/define-actions.xml"
        );

        if (isVersion(6)) {
            config.addPathsToAuxAtlassianPluginXMLs(
                    "/dynamic/v6-modules.xml"
            );
        }

        registrationHandle = dynamicModuleDescriptorFactory.loadModules(config);
    }

    void onEnd() {
        registrationHandle.unregister();
    }

After the call to `DynamicModuleDescriptorFactory` then modules will come into existence and be available to other 
parts of the system. 

You get a handle back on every call to `loadModules()`.  It is VERY important that you unregister this handle
on plugin shutdown otherwise you may leak resources.

Once you close the handle the modules are not longer in existence.

If you determine you are on a certain version then you could include extra configuration files say or the same technique 
could be used to decide what to do if you are licensed or not say.

The format of the dynamic modules xml files is the same as a normal atlassian-plugin.xml.

For example imagine a file called `/dynamic/define-servlets.xml` in your jar : 

        <?xml version="1.0" encoding="UTF-8"?>
        <atlassian-plugin key="your.plugin.key" plugins-version="2">
        
            <!--================================
                SERVLET FILTERS
            =================================-->
        
            <servlet-filter key="interceptor"
                            class="com.your.company.InterceptItFilter"
                            location="before-decoration">
                <url-pattern>/path/*</url-pattern>
        
                <dispatcher>REQUEST</dispatcher>
                <dispatcher>FORWARD</dispatcher>
            </servlet-filter>
            ...
        </atlassian-plugin>
 

# Contribution #

All pull requests are expected to have a green branch build in the [Pocketknife](https://servicedesk-bamboo.internal.atlassian.com/browse/PK-PK) plan.
Once the pull request is merged to master, you can manually release a version using
the [Pocketknife Release](https://servicedesk-bamboo.internal.atlassian.com/browse/PK-PKRLS) plan.